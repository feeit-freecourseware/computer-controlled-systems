%%% Simulacija na procesot od primerot 1
%%% so zadadeni vredosti na q0,q1,q2
%%% !!!!!!!! Preporaka e programot da go ispisete vo Notepad a ne direktno na komandnata %%% linija na MATLAB!!!!!
clear; % se brise celata rabotna memorija na MATLAB
q0=5.958; %ovie vrednosti se zadavaat za sekoja site razlicni periodi na odbiranje T
q1=-10.337;
q2=4.492;
a1=-1.68364;
a2=0.70469;
b1=-0.07289;
b2=0.09394;
T=1; % perioda na odbiranje
w=1.0; % edinece otskocen vlez
u_1=0; % minati vrednosti na upravuackata golemina
u_2=0; % vo odnos na momentot koga se vrsi presmetkata
y_1=0; % minati vrednosti na izlezot
y_2=0; % vo odnos na momentot koga se vrsi presmetkata
e_1=0; % minati vrednosti na greskata
e_2=0; % vo odnos na momentot koga se vrsi presmetkata
for n=1:128/T
y=-a1*y_1-a2*y_2+b1*u_1+b2*u_2; % diferentna ravenka
%na objektot na upravuvanje
e=w-y; % vrednost na greskata
u=u_1+q0*e+q1*e_1+q2*e_2; % diferentna ravenka na upravuvacot
e_2=e_1;
e_1=e;
u_2=u_1;
u_1=u;
y_2=y_1;
y_1=y;
Tvec(n)=(n-1)*T;
uvec(n)=u; % vektor vo koj se cuvaat site vrednosti
% na upravuvackiot signal za
% za vrednosti na vremeto od 0 do T
yvec(n)=y; % vektor vo koj se cuvaat site vrednosti
% na izlezniot signal za
% za vrednosti na vremeto od 0 do T
evec(n)=e; % vektor vo koj se cuvaat site vrednosti
% na greskata za
% vrednosti na vremeto od 0 do T
end
subplot(2,1,1),plot(Tvec,uvec,'b'); grid; % se iscrtuva upravuvackiot signal so sina linija
% niz sekoja tocka na ekranot na amplituda uvec, po oskata Tvec
subplot(2,1,2),plot(Tvec,yvec,'r'); grid; % se iscrtuva izlezot so crvena linija
% niz sekoja tocka na ekranot na amplituda yvec, po oskata Tvec
% ponataka mozat da se vrsat razlicni presmetki so vaka socuvanite vrednosti na y, e i u
Esum2=0; % suma od kvadratite na greskite
for i=1:128/T
Esum2=Esum2+(evec(i))^2;
end
Se2=(1/(128/T+1))*Esum2; % sredno kvadratno upravuvacko otstapuvanje
Se=((1/(128/T+1))*Esum2)^(1/2); % koren od srednoto kvadratno upravuvacko otstapuvanje
uBeskraj=uvec(128/T);
Delta_u_sum2=0;
for i=1:128/T
Delta_u_sum2=Delta_u_sum2+(uvec(i)-uBeskraj)^2;
end
Su2=(1/(128/T+1))*Delta_u_sum2;
Su=((1/(128/T+1))*Delta_u_sum2)^(1/2);
Seu=Se+Su;
ymax=max(yvec);