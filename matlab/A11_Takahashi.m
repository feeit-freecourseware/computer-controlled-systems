%%
numc = ;             % broitel na procesot
denc = ;             % imenitel na procesot
%%
Tu = ;  % parametar (dobien od identifikacijata)
Tg = ;  % parametar (dobien od identifikacijata)
Kp = ;  % parametar (dobien od identifikacijata)
%%
T0 = ;                % perioda na sempliranje
K0 = ;                % K0 parametar (dobien od dijagramite)
K = K0 / Kp;
Cd = ;               % Cd parametar (dobien od dijagramite)
Ci = ;               % Ci parametar (dobien od dijagramite)
%%
% Parametri na digitalniot upravuvac izvedeni od K, Cd i Ci: 
q0 = K*(1+Cd);          
q1 = K*(Ci-2*Cd-1);
q2 = K*Cd; 